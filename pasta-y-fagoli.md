# pasta y fagoli

easy traditional italian tomato based pasta and bean soup

recipe originally obtained from back of a box of ronzoni shells

## ingredients

* 1 box of medium shells (or similar)
* 1 onion chopped in about ½ “ pieces
* ~5 stalks of celery chopped in about ¼” slices
* 1 green pepper (optional)

  > can substitute any kind of pepper (red, pablano, italian, even jalapeno)

* ~6 oz of canadian bacon

  > can substitute regular bacon, turkey bacon, ham. i like canadian bacon because it is flavorful, yet low in fat

* 1 large can of crushed, petite diced or regular diced tomatoes
* 1 small can of tomato paste

  > can substitute 1 can of favorite spaghetti sauce for tomatoes and paste

* ~4 cups of luke warm water
* ~3 cloves of garlic (optional)
* 1 bayleaf (optional)
* 1 tsp of oregano (optional)
* salt and black pepper (optional to taste)

  > after you get a baseline that you like, can experiment with various other spices such as:
  >
  > * crushed red pepper
  > * cayenne
  > * cumin powder
  > * turmeric
  > * [cumin seeds](https://www.google.com/search?q=cumin+seeds)
  > * [mustard seeds (dark)](https://www.google.com/search?q=mustard+seed)

* 2 cans of kidney beans (discard liquid and rinse in strainer)

  > can substitute cannolini/white-northern beans

* powdered/shredded parmesan cheese (optional)
* fresh chopped parsley, scallions or cilantro (optional)

## directions

1.  Heat oil in large pot
1.  heat water for pasta in appropriate pot
1.  Add bacon on medium heat and cook until browned (~5 min)
1.  Add celery and onions (and green pepper if using) and lightly saute (~5 min)
1.  Add tomatos/sauce, water, dry herbs/spices, beans and bring to a simmering boil
1.  cook pasta in boiling water, drain and rinse pasta under cold tap water to stop cooking
1.  serve with pasta, optionally top with
1.  optionally garnish with parmesan cheese and/or fresh green herbs

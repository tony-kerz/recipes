# chicken curry

basic traditional indian curry

recipe originally taught to me by indian neighbors, i think i changed it over time based on less than perfect memory

## ingredients

* a couple of tbsp of cooking oil
* a couple of cloves of garlic minced
* a chunk of ginger (about the size of a cherry) minced
* about a cup of red onions chopped
* about a cup of tomatoes diced (can use canned)
* 1/2 tsp of [black mustard seed](https://en.wikipedia.org/wiki/Mustard_seed)
* 1/2 tsp of [cumin seed](https://en.wikipedia.org/wiki/Cumin)
* 1 tsp of [cumin powder](https://en.wikipedia.org/wiki/Cumin)
* 1 tsp of [turmeric powder](https://en.wikipedia.org/wiki/Turmeric)
* a few small [chili peppers](https://en.wikipedia.org/wiki/Chili_pepper) to taste (optional)
* salt to taste
* 1 lb of boneless chicken breast chopped into small (~1 in) pieces
* cilantro (optional, but recommended) chopped
* 1/2 cup of water
* two cups of rice cooked (basmati recommended, but can use jasmine or other)

## directions

1. start cooking rice per directions
1. heat oil in large pan over medium high heat
1. add garlic and ginger, let cook until turning golden brown (~1 min)
1. add mustard and cumin seeds
1. add onions, reduce heat to medium-low, stir well and occasionally until onions become soft (~10 min)
1. add chicken, increase heat to high, stir occasionally until chicken starts to brown (~5 min)
1. add chilis, tomatoes, salt, cumin and turmeric powders, water, stir, reduce heat to low and let simmer for at least 10 min
    > can cook longer, just add water if consistency gets too thick

1.  serve over rice
1.  optionally garnish with cilantro
